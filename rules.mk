# Boot Optimiziations and dedicated bootloader Key
BOOTMAGIC_ENABLE = yes      # Enable Bootmagic Lite
LTO_ENABLE = yes
# Debouce stuff
DEBOUNCE_TYPE = sym_defer_g
# Extra key features
MOUSEKEY_ENABLE = yes       # Mouse keys
EXTRAKEY_ENABLE = yes       # Audio control and System control
SPACE_CADET_ENABLE = no
GRAVE_ESC_ENABLE = no
SWAP_HANDS_ENABLE = no
# Debug stuff
CONSOLE_ENABLE = no        # Console for debug
COMMAND_ENABLE = no        # Commands for debug and configuration
# N-Key Rollover
NKRO_ENABLE = yes           # Enable N-Key Rollover
# Lighting
BACKLIGHT_ENABLE = no       # Enable keyboard backlight functionality
RGBLIGHT_ENABLE = no        # Enable keyboard RGB underglow
# Audio features
AUDIO_ENABLE = no          # Audio output
# AUDIO_DRIVER = dac_additive
# MUSIC_ENABLE = no

# RGB-Stuff
RGB_MATRIX_ENABLE = no

# RGB_MATRIX_DRIVER = IS31FL3731
# EEPROM_DRIVER = i2c

#project specific files
# SRC += matrix.c
# QUANTUM_LIB_SRC += i2c_master.c

# MOUSE_SHARED_EP = no
